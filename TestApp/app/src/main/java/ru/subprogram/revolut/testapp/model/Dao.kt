package ru.subprogram.revolut.testapp.model

import android.content.Context
import android.preference.PreferenceManager
import ru.subprogram.revolut.testapp.model.entity.Currency
import ru.subprogram.revolut.testapp.model.entity.ExchangeRates
import ru.subprogram.revolut.testapp.model.entity.Rate

class Dao(context: Context) {

	companion object {
		const val DATE_KEY = "DATE_KEY"
		const val RATES_KEY = "RATES_KEY"
		const val RATE_NAME_VALUE_SEPARATOR = "&"
	}

	private val preferenceManager = PreferenceManager.getDefaultSharedPreferences(context)

	fun saveRates(exchangeRates: ExchangeRates.Valid) {
		val base = exchangeRates.currency.name
		preferenceManager.edit()
				.putString("$DATE_KEY$base", exchangeRates.date)
				.apply()

		val set = exchangeRates.rates.map { "${it.currency.name}$RATE_NAME_VALUE_SEPARATOR${it.value}" }.toSet()
		preferenceManager.edit()
				.putStringSet("$RATES_KEY$base", set)
				.apply()
	}

	fun loadRates(currency: Currency) : ExchangeRates? {
		try {
			val date = preferenceManager.getString("$DATE_KEY${currency.name}", null) ?: return null

			val list = preferenceManager.getStringSet("$RATES_KEY${currency.name}", null)?.map {
				val pair = it.split(RATE_NAME_VALUE_SEPARATOR)
				Rate.Valid(pair[0], pair[1].toDouble())
			}?.toList() ?: return null

			return ExchangeRates.Valid(currency, date).apply {
				rates = list
			}
		}
		catch (e: Exception) {
			return null
		}
	}
}
