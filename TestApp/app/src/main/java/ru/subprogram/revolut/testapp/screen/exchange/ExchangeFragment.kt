package ru.subprogram.revolut.testapp.screen.exchange

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.*
import ru.subprogram.revolut.testapp.R
import ru.subprogram.revolut.testapp.model.entity.Error
import ru.subprogram.revolut.testapp.view.RatesPagerAdapter

class ExchangeFragment : Fragment() {

	private lateinit var viewModel: ExchangeViewModel

	private lateinit var acceptMenuItem: MenuItem

	private var isExchangeAllowed = false

	private var currentBalanceData: LiveData<Double>? = null

	private val mediator = MediatorLiveData<Data>()

	private var errorSnackbar: Snackbar? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {

		viewModel = ViewModelProviders.of(activity).get(ExchangeViewModel::class.java)

		val view = inflater.inflate(R.layout.fragment_main, container, false)

		val viewPagerFrom = initViewPager(view, R.id.pagerFromView)
		val viewPagerTo = initViewPager(view, R.id.pagerToView)

		viewPagerFrom.currentItem = 1       //TODO
		viewPagerTo.currentItem = 2         //TODO

		mediator.addSource(viewModel.value) { value ->
			value ?: return@addSource
			val currentBalance = mediator.value?.balance
			val currentError = mediator.value?.error
			mediator.value = Data(currentBalance, value, currentError)
		}

		val errorData = viewModel.repository.connectionError
		mediator.addSource(errorData) { error ->
			error ?: return@addSource
			val currentBalance = mediator.value?.balance
			val currentValue = mediator.value?.value
			mediator.value = Data(currentBalance, currentValue, error)
		}

		mediator.observe(this, Observer { data ->
			val value = data?.value ?: return@Observer
			val balance = data.balance ?: return@Observer
			val error = data.error ?: return@Observer
			isExchangeAllowed = value > 0 && value <= balance && error == Error.NoError
			ActivityCompat.invalidateOptionsMenu(activity)
		})

		errorData.observe(this, Observer { error ->
			if (error != Error.NoError) {
				errorSnackbar ?: kotlin.run {
					errorSnackbar = Snackbar.make(view, R.string.reconnecting, Snackbar.LENGTH_INDEFINITE).apply {
						setAction(R.string.hide) {
							dismiss()
						}
						show()
					}
					isExchangeAllowed = false
					ActivityCompat.invalidateOptionsMenu(activity)
				}
			}
			else {
				errorSnackbar?.dismiss()
				errorSnackbar = null
			}
		})
		return view
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater)
		inflater.inflate(R.menu.menu_exchange, menu)
		acceptMenuItem = menu.findItem(R.id.action_accept)
	}

	override fun onPrepareOptionsMenu(menu: Menu?) {
		super.onPrepareOptionsMenu(menu)

		acceptMenuItem.isEnabled = isExchangeAllowed
	}

	private fun initViewPager(view: View, @IdRes resourceId: Int): ViewPager {
		val isInverted = resourceId != R.id.pagerFromView

		val repository = viewModel.repository
		val pagerAdapter = RatesPagerAdapter(isInverted, repository, childFragmentManager)
		val viewPager = view.findViewById<ViewPager>(resourceId)
		viewPager.adapter = pagerAdapter

		viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
			override fun onPageSelected(position: Int) {
				val base = pagerAdapter.getCurrency(position)
				if (isInverted) {
					viewModel.currentCurrencyTo.value = base
					if (viewModel.isEditedValueInverted)
						viewModel.value.value = 0.0
				}
				else {
					viewModel.currentCurrencyFrom.value = base
					if (!viewModel.isEditedValueInverted)
						viewModel.value.value = 0.0
					val balanceData = repository.getBalance(base)
					currentBalanceData?.let {
						mediator.removeSource(it)
					}
					mediator.addSource(balanceData) { balance ->
						val currentValue = mediator.value?.value
						val currentError = mediator.value?.error
						mediator.value = Data(balance, currentValue, currentError)
					}

					currentBalanceData = balanceData
				}
			}
		})
		return viewPager
	}

	data class Data(val balance: Double?, val value: Double?, val error: Error?)
}
