package ru.subprogram.revolut.testapp.model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.os.Handler
import okhttp3.Call
import ru.subprogram.revolut.testapp.model.entity.Currency
import ru.subprogram.revolut.testapp.model.entity.Error
import ru.subprogram.revolut.testapp.model.entity.ExchangeRates
import ru.subprogram.revolut.testapp.model.entity.Rate
import ru.subprogram.revolut.testapp.model.livedata.InitedMutableLiveData
import ru.subprogram.revolut.testapp.model.network.NetworkConnectionLiveData
import ru.subprogram.revolut.testapp.model.parser.RatesParser
import ru.subprogram.revolut.testapp.model.livedata.ConstLiveData
import ru.subprogram.revolut.testapp.model.network.WebService

class Repository private constructor(context: Context) : IRepository {

	companion object {
		private const val RATES_URL = "http://api.fixer.io/latest?base="

		private const val REFRESH_RATE = 30_000L

		private var instance: IRepository? = null

		@JvmStatic
		fun getInstance(context: Context): IRepository = instance
				?: Repository(context.applicationContext)
	}

	private val handler = Handler()

	private val dao = Dao(context)

	private val webService = WebService()

	private val ratesMap = mutableMapOf<Currency, LiveData<ExchangeRates>>()

	private val balance = mapOf(
			"GBP" to InitedMutableLiveData(10.0),
			"USD" to InitedMutableLiveData(62.31),
			"EUR" to InitedMutableLiveData(5.05),
			"PLN" to InitedMutableLiveData(20.47),
			"CHF" to InitedMutableLiveData(0.0))

	override val availableCurrencies: List<Currency>
		get() = arrayListOf("GBP", "USD", "EUR", "PLN", "CHF")
				.map { Currency(it) }

	override val connectionError = MutableLiveData<Error>()

	override fun getRate(currencyFrom: Currency, currencyTo: Currency): LiveData<Rate> {
		if (currencyFrom == currencyTo) {
			return ConstLiveData(Rate.Valid(currencyFrom, 1.0))
		}

		val rates = Transformations.switchMap(NetworkConnectionLiveData) { isConnected ->
			if (isConnected) {
				ratesMap[currencyFrom] ?: RatesList(currencyFrom).apply {
					ratesMap[currencyFrom] = this
				}
			}
			else {
				val error = Error.NetworkNoConnectionError
				connectionError.postValue(error)
				val data: ExchangeRates = ExchangeRates.Invalid(error)
				InitedMutableLiveData(data)
			}
		}

		return Transformations.map(rates) { exchangeRates ->
			when (exchangeRates) {
				is ExchangeRates.Valid -> {
					exchangeRates.rates.firstOrNull { it.currency == currencyTo }
							?: Rate.Invalid(Error.NoDataError)
				}

				is ExchangeRates.Invalid ->
						Rate.Invalid(exchangeRates.error)
			}
		}
	}

	override fun getBalance(currency: Currency): LiveData<Double> = //TODO
			balance[currency.name] ?: ConstLiveData(0.0)

	inner class RatesList(private val currency: Currency) : MutableLiveData<ExchangeRates>() {

		private var currentCall: Call? = null

		private val parser = RatesParser()

		private val reloadDataTask = Runnable {
			reload()
		}

		override fun onActive() {
			super.onActive()

			val storedRates = dao.loadRates(currency)
			storedRates?.let {
				value = it
			}
			reloadDataTask.run()
		}

		private fun reload() {
			currentCall = webService.requestSmallText("$RATES_URL${currency.name}") { response ->
				when (response) {
					is WebService.WebServiceResponse.Invalid -> {
						if (currentCall?.isCanceled == false)
							connectionError.postValue(response.error)
					}

					is WebService.WebServiceResponse.Valid -> {
						connectionError.postValue(Error.NoError)
						val exchangeRates = parser.parse(response.result)
						dao.saveRates(exchangeRates)
						postValue(exchangeRates)
					}
				}
				currentCall = null
			}
			handler.postDelayed(reloadDataTask, REFRESH_RATE)
		}

		override fun onInactive() {
			super.onInactive()

			handler.removeCallbacks(reloadDataTask)

			currentCall?.cancel()
			currentCall = null
		}
	}
}

interface IRepository {
	val availableCurrencies: List<Currency>
	val connectionError: LiveData<Error>
	fun getRate(currencyFrom: Currency, currencyTo: Currency): LiveData<Rate>
	fun getBalance(currency: Currency): LiveData<Double>
}
