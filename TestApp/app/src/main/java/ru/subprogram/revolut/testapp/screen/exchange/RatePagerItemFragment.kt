package ru.subprogram.revolut.testapp.screen.exchange

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.subprogram.revolut.testapp.R
import ru.subprogram.revolut.testapp.model.entity.Currency
import ru.subprogram.revolut.testapp.model.entity.Rate
import ru.subprogram.revolut.testapp.utils.isZero
import ru.subprogram.revolut.testapp.utils.toLocalizedDouble
import ru.subprogram.revolut.testapp.utils.toLocalizedString
import ru.subprogram.revolut.testapp.view.WatchedEditText

class RatePagerItemFragment : Fragment() {

	companion object {

		private const val ARG_BASE = "ARG_BASE"

		private const val ARG_IS_INVERTED = "ARG_IS_INVERTED"

		fun newInstance(currency: Currency, isInverted: Boolean): RatePagerItemFragment {
			return RatePagerItemFragment().apply {
				val args = Bundle()
				args.putString(ARG_BASE, currency.name)
				args.putBoolean(ARG_IS_INVERTED, isInverted)
				arguments = args
			}
		}
	}

	private lateinit var viewModel: ExchangeViewModel

	private var baseTextView: TextView? = null

	private var valuePrefixTextView: TextView? = null

	private var valueEditText: WatchedEditText? = null

	private var balanceTextView: TextView? = null

	private var rateTextView: TextView? = null

	private val currencyFrom by lazy { Currency(arguments.getString(ARG_BASE)) }

	private val isInverted by lazy { arguments.getBoolean(ARG_IS_INVERTED) }

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
	                          savedInstanceState: Bundle?): View? {

		viewModel = ViewModelProviders.of(activity).get(ExchangeViewModel::class.java)

		val view = inflater.inflate(R.layout.fragment_rate_page_item, container, false)

		baseTextView = view.findViewById(R.id.baseTextView)
		balanceTextView = view.findViewById(R.id.balanceTextView)
		rateTextView = view.findViewById(R.id.rateTextView)
		valuePrefixTextView = view.findViewById(R.id.valuePrefixEditText)
		valueEditText = view.findViewById(R.id.valueEditText)

		valueEditText?.addTextChangedListener(object : TextWatcher {
			override fun afterTextChanged(editable: Editable?) {
				val isEditedByUser = valueEditText?.isEditedByUser ?: return
				if (!isEditedByUser)
					return

				viewModel.isEditedValueInverted = isInverted
				val value = editable.toString().toLocalizedDouble()

				viewModel.value.value = value
			}

			override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

			override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
		})

		valuePrefixTextView?.text = if (isInverted) "+" else "-"

		viewModel.getModelData(currencyFrom, isInverted).observe(this, Observer { model ->
			update(model)
		})

		return view
	}

	override fun onDestroyView() {
		super.onDestroyView()

		baseTextView = null
		balanceTextView = null
		rateTextView = null
		valuePrefixTextView = null
		valueEditText = null
	}

	private fun update(data: ExchangeViewModel.Model?) {
		val rate = data?.rate ?: return
		val balance = data.balance ?: return
		val value = data.value ?: return
		val editText = valueEditText ?: return

		baseTextView?.text = currencyFrom.name
		balanceTextView?.text = getString(R.string.you_have_, currencyFrom.sign, data.balance)
		balanceTextView?.isActivated = !isInverted && value > balance

		if (viewModel.isEditedValueInverted == !isInverted) {
			editText.setText(if (rate is Rate.Valid && !value.isZero())
				(value * rate.value).toLocalizedString()
			else
				"0")
		}
		else if (editText.text.toString().toLocalizedDouble() != value) {
			editText.setText(value.toLocalizedString())
		}

		valuePrefixTextView?.visibility = if (value.isZero())
			View.GONE
		else
			View.VISIBLE

		if (rate is Rate.Valid && rate.currency != currencyFrom) {
			rateTextView?.apply {
				text = getString(R.string.exchange_string_pattern,
						currencyFrom.sign, rate.currency.sign, rate.value)
				visibility = View.VISIBLE
			}
			balanceTextView?.visibility = View.VISIBLE
		}
		else {
			rateTextView?.visibility = View.GONE
			balanceTextView?.visibility = View.GONE
		}
	}
}
