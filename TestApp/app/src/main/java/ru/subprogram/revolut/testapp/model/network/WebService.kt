package ru.subprogram.revolut.testapp.model.network

import okhttp3.*
import ru.subprogram.revolut.testapp.model.entity.Error
import java.io.IOException
import java.util.concurrent.TimeUnit


class WebService : IWebService {

	private val client: OkHttpClient = OkHttpClient.Builder()
			.connectTimeout(60000, TimeUnit.MILLISECONDS)
			.readTimeout(60000, TimeUnit.MILLISECONDS)
			.writeTimeout(60000, TimeUnit.MILLISECONDS)
			.build()

	override fun requestSmallText(url: String, onComplete: (WebServiceResponse) -> Unit): Call {
		val request = Request.Builder()
				.url(url)
				.build()

		return client.newCall(request).apply {
			enqueue(object : Callback {
				override fun onFailure(call: Call, throwable: IOException) {
					val response = WebServiceResponse.Invalid(Error.NetworkUnexpectedError)
					onComplete(response)
				}

				override fun onResponse(call: Call, response: Response) {
					if (!response.isSuccessful) {
						val response = WebServiceResponse.Invalid(Error.NetworkUnexpectedError)
						onComplete(response)
						return
					}

					val result = response.body()?.string()
					val response = if (result != null)
						WebServiceResponse.Valid(result)
					else
						WebServiceResponse.Invalid(Error.NetworkEmptyResultError)
					onComplete(response)
				}
			})
		}
	}

	sealed class WebServiceResponse {
		data class Invalid(val error: Error) : WebServiceResponse()

		data class Valid(val result: String) : WebServiceResponse()
	}
}

interface IWebService {
	fun requestSmallText(url: String, onComplete: (WebService.WebServiceResponse) -> Unit): Call
}
