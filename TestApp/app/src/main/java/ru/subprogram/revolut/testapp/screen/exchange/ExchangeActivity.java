package ru.subprogram.revolut.testapp.screen.exchange;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import ru.subprogram.revolut.testapp.R;
import ru.subprogram.revolut.testapp.model.network.NetworkConnectionLiveData;

public class ExchangeActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initNetworkConnectionLiveData();    //TODO

		setContentView(R.layout.activity_main);
		final Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	}

	private void initNetworkConnectionLiveData() {
		final ConnectivityManager connectivityManager =
				(ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		boolean isConnected = networkInfo != null && networkInfo.isConnected();
		NetworkConnectionLiveData.INSTANCE.setValue(isConnected);
	}
}
