package ru.subprogram.revolut.testapp.view

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import ru.subprogram.revolut.testapp.model.IRepository
import ru.subprogram.revolut.testapp.model.entity.Currency
import ru.subprogram.revolut.testapp.screen.exchange.RatePagerItemFragment

class RatesPagerAdapter(private val isInverted: Boolean,
                        repository: IRepository,
                        fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

	private val currencies = repository.availableCurrencies

	fun getCurrency(position: Int): Currency = currencies[position]

	override fun getItem(position: Int): Fragment =
			RatePagerItemFragment.newInstance(getCurrency(position), isInverted)

	override fun getCount(): Int = currencies.count()
}
