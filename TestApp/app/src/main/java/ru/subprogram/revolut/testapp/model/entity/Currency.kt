package ru.subprogram.revolut.testapp.model.entity

data class Currency(val name: String) {

	val sign = when (name) {
		"USD" -> "$"

		"EUR" -> "€"

		"GBP" -> "£"

		"PLN" -> "zł"

		"CHF" -> "Fr"

		else -> throw IllegalArgumentException("Unknown currency $name")
	}
}
