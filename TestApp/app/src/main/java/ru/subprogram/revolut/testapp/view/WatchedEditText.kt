package ru.subprogram.revolut.testapp.view

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.widget.EditText

class WatchedEditText : EditText {
	constructor(context: Context) : super(context)

	constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

	constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

	constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int)
			: super(context, attrs, defStyleAttr, defStyleRes)

	var isEditedByUser = true

	override fun onRestoreInstanceState(state: Parcelable?) {
		isEditedByUser = false
		super.onRestoreInstanceState(state)
		isEditedByUser = true
	}

	override fun setText(text: CharSequence?, type: BufferType?) {
		isEditedByUser = false
		super.setText(text, type)
		isEditedByUser = true
	}
}
