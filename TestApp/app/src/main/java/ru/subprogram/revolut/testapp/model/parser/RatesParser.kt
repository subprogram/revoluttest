package ru.subprogram.revolut.testapp.model.parser

import org.json.JSONException
import org.json.JSONObject
import ru.subprogram.revolut.testapp.model.entity.Currency
import ru.subprogram.revolut.testapp.model.entity.ExchangeRates
import ru.subprogram.revolut.testapp.model.entity.Rate

class RatesParser {

	companion object {
		private const val BASE_KEY = "base"
		private const val DATE_KEY = "date"
		private const val RATES_KEY = "rates"
	}

	@Throws(JSONException::class)
	fun parse(text: String): ExchangeRates.Valid {
		val jsonResult = JSONObject(text)
		val base = jsonResult.getString(BASE_KEY)
		val date = jsonResult.getString(DATE_KEY)
		val rates = jsonResult.getJSONObject(RATES_KEY)
		val result = ExchangeRates.Valid(Currency(base), date)
		result.rates = rates.keys().asSequence().mapNotNull {
			val value = rates.getDouble(it)
			try {
				Rate.Valid(it, value)
			}
			catch (e: IllegalArgumentException) {
				null
			}
		}.toList()
		return result
	}
}
