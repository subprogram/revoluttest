package ru.subprogram.revolut.testapp.model.entity

enum class Error {
	NoError,
	NoDataError,
	NetworkNoConnectionError,
	NetworkEmptyResultError,
	NetworkUnexpectedError
}
