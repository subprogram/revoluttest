package ru.subprogram.revolut.testapp.model.entity

sealed class Rate {
	data class Invalid(val error: Error) : Rate()

	data class Valid(val currency: Currency, val value: Double) : Rate() {
		constructor(currencyName: String, value: Double) : this(Currency(currencyName), value)
	}
}
