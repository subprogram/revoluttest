package ru.subprogram.revolut.testapp.model.network

import android.arch.lifecycle.MutableLiveData

object NetworkConnectionLiveData : MutableLiveData<Boolean>()
