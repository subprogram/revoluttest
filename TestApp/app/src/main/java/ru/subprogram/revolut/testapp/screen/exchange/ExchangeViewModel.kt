package ru.subprogram.revolut.testapp.screen.exchange

import android.app.Application
import android.arch.lifecycle.*
import ru.subprogram.revolut.testapp.model.Repository
import ru.subprogram.revolut.testapp.model.entity.Currency
import ru.subprogram.revolut.testapp.model.entity.Rate
import ru.subprogram.revolut.testapp.model.livedata.InitedMutableLiveData

class ExchangeViewModel(application: Application) : AndroidViewModel(application) {

	val repository = Repository.getInstance(application)

	val currentCurrencyFrom = MutableLiveData<Currency>()

	val currentCurrencyTo = MutableLiveData<Currency>()

	val value = InitedMutableLiveData(0.0)

	var isEditedValueInverted = false

	fun getModelData(currencyFrom: Currency, isInverted: Boolean): LiveData<Model> {
		val currencyTo = if (isInverted)
			currentCurrencyFrom
		else
			currentCurrencyTo

		val rateData = Transformations.switchMap(currencyTo) { currency ->
			repository.getRate(currencyFrom, currency)
		}

		val balanceData = repository.getBalance(currencyFrom)

		val mediator = MediatorLiveData<Model>()
		mediator.addSource(rateData) { rate ->
			val currentBalance = mediator.value?.balance
			val currentValue = mediator.value?.value
			mediator.value = Model(rate, currentBalance, currentValue)
		}

		mediator.addSource(balanceData) { balance ->
			val currentRate = mediator.value?.rate
			val currentValue = mediator.value?.value
			mediator.value = Model(currentRate, balance, currentValue)
		}

		mediator.addSource(value) { value ->
			val currentRate = mediator.value?.rate
			val currentBalance = mediator.value?.balance
			mediator.value = Model(currentRate, currentBalance, value)
		}
		return mediator
	}

	data class Model(val rate: Rate?, val balance: Double?, val value: Double?)
}
