package ru.subprogram.revolut.testapp.utils

import java.text.NumberFormat
import java.text.ParseException
import java.util.*

fun String.toLocalizedDouble(): Double {
	return try {
		val nf = NumberFormat.getInstance(Locale.getDefault())
		nf.parse(this.replace("\u00A0", "")).toDouble()
	}
	catch (e: ParseException) {
		0.0
	}
}

fun Double.toLocalizedString(fractionDigitsCount: Int = 2): String {
	val nf = NumberFormat.getInstance(Locale.getDefault())
	nf.minimumFractionDigits = 0
	nf.maximumFractionDigits = fractionDigitsCount
	return nf.format(this)
}

fun Double.isZero(): Boolean = this < 0.001
