package ru.subprogram.revolut.testapp.model.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager


class ConnectionChangeReceiver : BroadcastReceiver() {

	override fun onReceive(context: Context, intent: Intent?) {
		val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		NetworkConnectionLiveData.value = connectivityManager.activeNetworkInfo?.isConnected ?: false
	}
}
