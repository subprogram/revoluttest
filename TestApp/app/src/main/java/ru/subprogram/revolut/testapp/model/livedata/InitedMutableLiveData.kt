package ru.subprogram.revolut.testapp.model.livedata

import android.arch.lifecycle.MutableLiveData

class InitedMutableLiveData<T>(initialValue: T) : MutableLiveData<T>() {
	init {
		value = initialValue
	}
}
