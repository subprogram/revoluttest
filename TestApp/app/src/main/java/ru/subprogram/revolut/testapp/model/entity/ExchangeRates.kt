package ru.subprogram.revolut.testapp.model.entity

sealed class ExchangeRates {
	data class Invalid(val error: Error) : ExchangeRates()

	class Valid(var currency: Currency, var date: String) : ExchangeRates() {
		var rates: List<Rate.Valid> = arrayListOf()
	}
}
