package ru.subprogram.revolut.testapp.model.livedata

import android.arch.lifecycle.LiveData

class ConstLiveData<T>(value: T) : LiveData<T>() {
	init {
		super.setValue(value)
	}
}
